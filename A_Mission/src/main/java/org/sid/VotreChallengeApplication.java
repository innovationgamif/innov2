package org.sid;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import org.sid.dao.*;
import org.sid.entities.*;
import org.sid.metier.Imissionmetier;

@SpringBootApplication
//@EnableJpaRepositories
@EnableJpaRepositories("org.sid.dao")

@Repository
public class VotreChallengeApplication implements CommandLineRunner{

	@Autowired
	private CollaborateurRepository collaborateurRepository;
	@Autowired
	private ChallengeRepository challengeRepository;
	@Autowired
	private RecompenseRepository recompenseRepository;
	@Autowired
	private GroupeRepository groupeRepository;
	@Autowired
	private MissionRepository missionRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private OperationRepository operationRepository;
	@Autowired
	private TacheRepository tacheRepository;
	
	@Autowired
	private Imissionmetier missionMetier;//pour tester les methodes implémenter
	
	public static void main(String[] args) {
		/*ApplicationContext ctx = SpringApplication.run(VotreChallengeApplication.class, args);
		CollaborateurRepository collaborateurRepository = ctx.getBean(CollaborateurRepository.class);
		collaborateurRepository.save(new Collaborateur("omar","R.omar@gmail.com"));//deja ajouter
		Recompense c=new Formation();
		c.setStock(10);
		ChallengeRepository challengeRepository = ctx.getBean(ChallengeRepository.class);
		challengeRepository.save(new Challenge("ik","ff","",new Date(),new Date(),"",c));
		List<Challenge> Challenge=challengeRepository.findAll();
		 System.out.println(Challenge);
		RecompenseRepository recompenseRepository = ctx.getBean(RecompenseRepository.class);
		recompenseRepository.save(new Recompense());
		
		GroupeRepository groupeRepository = ctx.getBean(GroupeRepository.class);
		groupeRepository.save(new Groupe(""));
		Groupe g = new Groupe("Prolog");
		g.setCodeGroupe(123);
		g.setNomGroupe("BCX");*/
		SpringApplication.run(VotreChallengeApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		/*Collaborateur c1 = collaborateurRepository.save(new Collaborateur("ali","A.jawad@cgi.com"));
		Challenge ch1 =  challengeRepository.save(new Challenge("titre","statut","type","description", new Date(),new Date()));
		Challenge ch2 =  challengeRepository.save(new Challenge());//des valeurs null
		Groupe g1 = groupeRepository.save(new Groupe("G-Prolog"));
		
		Utilisateur u1=utilisateurRepository.save(new Utilisateur("BOUAZIZ","FatimaEzzahra","bouaziz.f@gmail.com"));
		Utilisateur u2=utilisateurRepository.save(new Utilisateur("AZOUZ","Omar","Azouz.Omar@gmail.com"));
		Mission m1=missionRepository.save(new Mission ("m1",new Date(),"en cours de réalisation",u1));
		Mission m2=missionRepository.save(new Mission ("m2",new Date(),"en début",u2));
		Operation op1=operationRepository.save(new Affectation(new Date(),m1));
		missionMetier.Affecter("m3",new Date(),"fin", u2); 
		missionMetier.consulterMission("m1"); 
		missionMetier.Affectertache("t2","daily du jour",new Date(),"13s",12,u2,m2);*/
		//Challenge ch1 =  challengeRepository.save(new Challenge("Ch_Prod2","en cours","mission","supply", new Date(),new Date()));

	}
}
