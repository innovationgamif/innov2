package org.sid.metier;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.sid.dao.ChallengeRepository;
import org.sid.dao.CollaborateurRepository;
import org.sid.dao.GroupeRepository;
import org.sid.dao.MissionRepository;
import org.sid.dao.RecompenseRepository;
import org.sid.dao.TacheRepository;
import org.sid.dao.OperationRepository;
import org.sid.dao.UtilisateurRepository;
import org.sid.entities.Affectation;
import org.sid.entities.Challenge;
import org.sid.entities.Collaborateur;
import org.sid.entities.Groupe;
import org.sid.entities.Mission;
import org.sid.entities.Operation;
import org.sid.entities.Recompense;
import org.sid.entities.Tache;
import org.sid.entities.Utilisateur;
import org.springframework.data.domain.PageRequest;
//utiliser pour les objets de la couche métier 
@Service
@Transactional 
public class MissionMetierImp implements Imissionmetier{
	private CollaborateurRepository collaborateurRepository;
	@Autowired
	private RecompenseRepository recompenseRepository;
	@Autowired
	private GroupeRepository groupeRepository;
	@Autowired
	private ChallengeRepository challengeRepository;
	
	@Autowired
	private MissionRepository missionRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private OperationRepository operationRepository;
	@Autowired
	private TacheRepository tacheRepository;
	
	@Override
	public Challenge consulterChallenge(Long codeChg) {
		System.out.println("ikram");
		Challenge challenge = challengeRepository.findOne(codeChg);
		//Challenge chg = challengeRepository.findOneById(codeChg).get();
		System.out.println("test"+"--"+challenge+"--"+codeChg);
		if(challenge==null) throw new RuntimeException("Challenge introuvable chg est null");
		return challenge;
	}
	
	@Override
	public Collaborateur consulterCollaborateur(Long codeCol) {
		System.out.println("iiii");
		Collaborateur collaborateur= collaborateurRepository.findOne(codeCol);
		System.out.println("test"+"..."+collaborateur.getEmail()+"--"+codeCol);
		if(collaborateur==null) throw new RuntimeException("Challenge introuvable col est null");
		return collaborateur;
	}

	/*@Override
	public Challenge deleteChallenge(Long codeChg) {
		System.out.println("delete");
		Challenge challenge= challengeRepository.findOne(codeChg);
		System.out.println(challenge + " was finded");
		challengeRepository.delete(codeChg);
		System.out.println("It was deleted");
		if(challenge==null) throw new RuntimeException("Challenge introuvable challenge est null");
		return null;
	}*/
	
	/*@Override
	public Page<Recompense> listRecompense(Long codeRec, int page, int size) {
		//return null;
		return recompenseRepository.listRecompense(codeRec, new PageRequest(page, size));
	}
	*/
	@Override
	public void affectationChallenge(Long codeChg, Long codeGrp) {
		
	}

	@Override
	public Groupe consulterGroupe(String nomGrp) {
		return null;
	}

	@Override
	public void affectationCollaborateur(Long matricule, String nomGrp) {
		
	}

	@Override
	public Recompense consulterRecompense(Long codeRec) {
		
		return null;
	}

	@Override
	public void affectationRecompense(Long codeRecompense, Long codeChg) {
		
		
	}

	@Override
	public Page<Recompense> listRecompense(Long codeRec, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Affectertache(String statut, String description,
			Date date_debut, String titre, String type, Date date_fin) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Mission consulterMission(String codeMission) {
		Mission mission=missionRepository.findOne(codeMission);
		if (mission==null) throw new RuntimeException("Mission introuvable");
		return mission;
		
	}

	@Override
	public void Affecter(String codeMission, Date date, String etat,
			Utilisateur utilisateur) {
		missionRepository.save(new Mission(codeMission,new Date(),etat,utilisateur));
		
	}

	@Override
	public Utilisateur consulterUtil(Long code) {
		Utilisateur u1=utilisateurRepository.getOne(code);
		if (u1==null) throw new RuntimeException("Mission introuvable");
		return null;
		
	}

	@Override
	public Tache consultertache(Long code) {
		Tache tache=tacheRepository.findOne(code);
		if (tache==null) throw new RuntimeException("Vous n'avez pas encore créer de tache pour cette mission");
		return tache;
	}

	@Override
	public void Affectertache(String nom, String descriptif, Date dateCreation,
			String temp,int pctg_acehvés, Utilisateur utilisateur, Mission mission) {
		tacheRepository.save(new Tache( nom,descriptif,new Date(),temp,pctg_acehvés,utilisateur,mission));
		
	}

	@Override
	public Page<Tache> listTache(String codeMission, int page, int size) {
		return tacheRepository.listTache(codeMission, new PageRequest(page, size));
	}

}
