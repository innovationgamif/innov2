package org.sid.metier;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.sid.entities.Challenge;
import org.sid.entities.Collaborateur;
import org.sid.entities.Groupe;
import org.sid.entities.Mission;
import org.sid.entities.Operation;
import org.sid.entities.Recompense;
import org.sid.entities.Tache;
import org.sid.entities.Utilisateur;
import org.sid.dao.MissionRepository;

public interface Imissionmetier {
	

	public Challenge consulterChallenge(Long codeChg);
	//public Challenge deleteChallenge(Long codeChg);
	public void affectationChallenge(Long codeChg, Long codeGrp);
	
	public Groupe consulterGroupe(String nomGrp);
	
	public Collaborateur consulterCollaborateur(Long matricule);
	public void affectationCollaborateur(Long matricule, String nomGrp);
	
	// Motivation consulterMotivation(Long matricule);
	
	public Recompense consulterRecompense(Long codeRec);
	public void affectationRecompense(Long codeRecompense, Long codeChg);
	
	//PAge est un interface offré par springData pour affichage des liste
	public Page<Recompense> listRecompense(Long codeRec, int page, int size);
	
	public void Affectertache(String statut, String description,Date date_debut,String titre,
			String type, Date date_fin);//,Challenge challenge
	
	public  Mission consulterMission(String codeMission);
	public void Affecter(String codeMission,Date date,String etat,Utilisateur utilisateur);
	public Utilisateur consulterUtil(Long code);
	public Tache consultertache(Long code);
	public void Affectertache(String nom, String descriptif, Date dateCreation,
			String temp, int pctg_acehvés, Utilisateur utilisateur,
			Mission mission);
	public Page<Tache> listTache(String codeMission, int page, int size);
}
