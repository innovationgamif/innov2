package org.sid.dao;

import org.sid.entities.Recompense;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RecompenseRepository extends JpaRepository<Recompense, Long> {

	//@Query("select o from Recompense o where o.challenge.codeChg=:x order by o.stock desc")
	// o = object recompense -- affiche de recompense parraport a un challenge
	//Page<Recompense> listRecompense(Long codeRec, Pageable pageable);

}
