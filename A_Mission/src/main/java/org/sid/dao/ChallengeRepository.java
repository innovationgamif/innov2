package org.sid.dao;

import java.util.Optional;

import org.sid.entities.Challenge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChallengeRepository extends JpaRepository<Challenge, Long>{
	
	Optional<Challenge> findOneById(Long id);

	void delete(Long id);

	void deleteById(Long codeChg);
}
