package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long>{

}
