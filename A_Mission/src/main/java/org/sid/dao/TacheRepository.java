package org.sid.dao;

import org.sid.entities.Tache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TacheRepository extends JpaRepository<Tache,Long>{
	
	@Query("select o from Tache o where o.mission.codeMission =:x order by o.dateCreation desc")
	public Page<Tache> listTache(@Param("x")String codeMission, Pageable pageable);

}
