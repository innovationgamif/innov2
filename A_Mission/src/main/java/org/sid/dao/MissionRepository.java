package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entities.Mission;

public interface MissionRepository extends JpaRepository<Mission,String>{

}
