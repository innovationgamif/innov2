package org.sid.dao;

import org.sid.entities.Challenge;
import org.sid.entities.Groupe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupeRepository extends JpaRepository<Challenge, Long>{

	Groupe save(Groupe groupe);

}
