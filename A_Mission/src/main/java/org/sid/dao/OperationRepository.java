package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entities.Operation;

public interface OperationRepository extends JpaRepository<Operation,Long>{

}
