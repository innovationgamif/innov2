package org.sid.web;
import java.util.Date;

import org.sid.dao.*;
import org.sid.entities.*;
import org.sid.metier.Imissionmetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MissionController {

	@Autowired
	private Imissionmetier missionmetier;
	@Autowired
	private Imissionmetier missionMetier;
	@Autowired
	private MissionRepository missionRepository;
	@Autowired
	private ChallengeRepository challengeRepository;
	
	@RequestMapping("/consulterChallenge")
	public String consulter(Model model, Long codeChg){
		
		try{
			Challenge challenge = missionmetier.consulterChallenge(codeChg);
			System.out.println(challenge+"----------------------");
			model.addAttribute("challenge",challenge);
		}catch(Exception e){
			System.out.println("Exception--------------------------");
			model.addAttribute("Exception",e);
			System.out.println(e.toString());
		}
		return "challenges";	
	}
	
	@RequestMapping("/consulterCollaborateur")
	public String consulterCollab(Model model, Long codeCol){
		
		try{
			Collaborateur collaborateur = missionmetier.consulterCollaborateur(codeCol);
			System.out.println(collaborateur.getNom()+"---------------------");
			model.addAttribute("colab",collaborateur);
		}catch(Exception e){
			System.out.println("--------Exception-------");
			model.addAttribute("Exception",e);
			System.out.println(e.toString());
		}
		return "collaborateurs";	
	}

	@RequestMapping("/listRecompense")
	public String listRecompense(Model model, Long codeRec){
		model.addAttribute("Rrecompense", codeRec);
		try{
			Recompense recompense = missionmetier.consulterRecompense(codeRec);
		    Page<Recompense> pageRecompenses=
		    		missionmetier.listRecompense(codeRec, 0, 4);
		    model.addAttribute("recompense",recompense.getNumero());
		    model.addAttribute("recompense", recompense);
		}catch(Exception e){
			model.addAttribute("------Exception------",e);
		}
		return "recompenses";
	}
	
	@RequestMapping("/challenges")
	public String Challenge(Model model){
		model.addAttribute("ch",new org.sid.entities.Challenge());
		return "challenges";
	}

	@RequestMapping("/addchallenges")
	public String AddChallenge(Model model,
		@RequestParam(name="statut")String S, String titre,
		String Description,
		Date dateDebut,
		Date dateFin,
		String type){
		//String statut, String description,Date date_debut,String titre,
		//String type, Date date_fin){
		System.out.println("test-----------------------------------------"+S);
		missionMetier.Affectertache(S, Description, new Date(), titre, type, new Date());
		try{
			//System.out.println(date_debut+"-----------"+date_fin);
			//challengeMetier.Affectertache(statut, description, date_debut, titre, type, date_fin);
			
		} catch(Exception e) {
			model.addAttribute("error", e);
			return "redirect:/consulterMission?codeMission="+
					"&error="+e.getMessage();
		}
		
		return "redirect:/consulterChallenge?codeChg=";
	}
	
	@RequestMapping(value="/saveChallenge", method=RequestMethod.POST)
	public String saveOperation(Model model){
			//String statut, String description,Date date_debut,String titre,
			//String type, Date date_fin){
		System.out.println("test-----------------------------------------");
		try{
			//System.out.println(date_debut+"-----------"+date_fin);
			//missionmetier.Affectertache(statut, description, date_debut, titre, type, date_fin);
			
		} catch(Exception e) {
			model.addAttribute("error", e);
			return "redirect:/consulterMission?codeMission="+
					"&error="+e.getMessage();
		}
		
		return "redirect:/consulterChallenge?codeChg=";
	}
		
	@RequestMapping("/deleteChallenge")
	public String delete(Model model, Long codeChg){
		
		try{
			System.out.println("Deleting challenge with id = " + codeChg);
			//Challenge challenge;// = missionmetier.deleteChallenge(codeChg);
			challengeRepository.deleteById(codeChg);
			System.out.println(codeChg+"----------------------");
			model.addAttribute("challenge",codeChg);
		}catch(Exception e){
			System.out.println("delete--Exception-- "+codeChg);
		}
		return "deleteChallenges";	
	}

	@RequestMapping("/deleteChallenges")
	public String deleteCh(){
		return "deleteChallenges";
	}
	@RequestMapping("/missions")
	public String index(){
		return "missions";
	}
	@RequestMapping("/template")
	public String index2(){
		return "template";
	}
	@RequestMapping("/dashboard")
	public String dashboard(){
		return "dashboard";
	}
	@RequestMapping("/recompenses")
	public String Recompense(){
		return "recompenses";
	}
	@RequestMapping("/collaborateurs")
	public String Collaborateur(){
		return "collaborateurs";
	}
	
	@RequestMapping("/index")
	public String Index(){
		return "index";
	}
	@RequestMapping("/consulterMission")
	public String Consulter(Model model,String codeMission,
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="size", defaultValue="5") int size){
		try{
		
		Mission mission=missionmetier.consulterMission(codeMission);
		model.addAttribute("mission", mission);
		Page<Tache> pageTaches=missionmetier.listTache(codeMission, page, size);
		model.addAttribute("listOperations", pageTaches.getContent());
		int[] pages = new int [pageTaches.getTotalPages()];
		model.addAttribute("pages",pages);
		} catch (Exception e) {
			model.addAttribute("exception",e);
		}
		return "missions";
	}
	@RequestMapping(value="/saveTache", method=RequestMethod.POST)
	public String saveOperation(Model model, 
			String nom, String descriptif,Date dateCreation,
			String temp,int  pctg_acehvés , Utilisateur utilisateur,Mission mission){
		
		try{
			missionmetier.Affectertache(nom, descriptif, new Date(), temp, pctg_acehvés, utilisateur, mission);
			
		} catch(Exception e) {
			model.addAttribute("error", e);
			return "redirect:/consulterMission?codeMission="+mission+
					"&error="+e.getMessage();
		}
		
		return "redirect:/consulterMission?codeMission="+mission;
	}

}
