package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Tache implements Serializable {
	@Id @GeneratedValue
	private Long code;
	private String nom;
	private String descriptif;
	private Date dateCreation;
	private String temp;
	private int  pctg_acehvés;
	@ManyToOne
	@JoinColumn(name="code_util")
	private Utilisateur utilisateur;
	@ManyToOne
	@JoinColumn(name="CODE_MISSION")
	private Mission mission;
		
	public Tache(String nom, String descriptif, Date dateCreation, String temp,
			int pctg_acehvés, Utilisateur utilisateur, Mission mission) {
		super();
		this.nom = nom;
		this.descriptif = descriptif;
		this.dateCreation = dateCreation;
		this.temp = temp;
		this.pctg_acehvés = pctg_acehvés;
		this.utilisateur = utilisateur;
		this.mission = mission;
	}


	public Utilisateur getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}


	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	public Tache() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescriptif() {
		return descriptif;
	}
	public void setDescriptif(String descriptif) {
		this.descriptif = descriptif;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
	public int getPctg_acehvés() {
		return pctg_acehvés;
	}
	public void setPctg_acehvés(int pctg_acehvés) {
		this.pctg_acehvés = pctg_acehvés;
	}
	
	

}
