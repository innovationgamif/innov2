package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public  class Challenge implements Serializable{

	@Id @GeneratedValue
	private Long id;
	private String titre;
	private String Statut;
	private String Description;
	private Date dateDebut;
	private Date dateFin;
	private String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@OneToOne
	private Recompense recompense;
	
	@OneToMany 
	private Collection<Groupe> groupe;
	
	public Long getID() {
		return id;
	}

	public void setID(Long id) {
		this.id = id;
	}
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getStatut() {
		return Statut;
	}

	public void setStatut(String statut) {
		Statut = statut;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public Challenge() {
		super();
	}

	public Challenge(String titre, String statut, String type, String description,
			Date dateDebut, Date dateFin) {
		super();
		
		this.titre = titre;
		this.type = type;
		Statut = statut;
		Description = description;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public Challenge(String titre, String statut, String description,
			Date dateDebut, Date dateFin, String type, Recompense recompense) {
		super();
		this.titre = titre;
		Statut = statut;
		Description = description;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.type = type;
		this.recompense = recompense;
	}

}

