package org.sid.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity 
public class Motivation implements Serializable{
	
	@Id @GeneratedValue
	private int id ;
	private String motivation;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMotivation() {
		return motivation;
	}

	public void setMotivation(String motivation) {
		this.motivation = motivation;
	}

	public Motivation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Motivation(int id, String motivation) {
		super();
		this.id = id;
		this.motivation = motivation;
	}
	

}
