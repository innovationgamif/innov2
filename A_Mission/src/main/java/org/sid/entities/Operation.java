package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="Type_OP",discriminatorType=DiscriminatorType.STRING,length=1)

public abstract class Operation implements Serializable{
	@Id @GeneratedValue
	private Long numero;
	private Date DateOperation;
	@ManyToOne
	@JoinColumn(name="CODE_MISSION")
	private Mission mission;
	public Operation(Date dateOperation, Mission mission) {
		super();
		DateOperation = dateOperation;
		this.mission = mission;
	}
	public Operation() {
		super();
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public Date getDateOperation() {
		return DateOperation;
	}
	public void setDateOperation(Date dateOperation) {
		DateOperation = dateOperation;
	}
	
	public Mission getMission() {
		return mission;
	}
	public void setMission(Mission mission) {
		this.mission = mission;
	}
	

}
