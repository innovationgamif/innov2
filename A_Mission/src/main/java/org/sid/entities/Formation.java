package org.sid.entities;

import java.util.Date;
import javax.persistence.Entity;

@Entity
public class Formation extends Recompense{

	private Date dateDebut;
	private Date dateFin;
	private int nbrLimite;
	private String sujet;
	private String tuteur;
	
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public int getNbrLimite() {
		return nbrLimite;
	}
	public void setNbrLimite(int nbrLimite) {
		this.nbrLimite = nbrLimite;
	}
	public String getSujet() {
		return sujet;
	}
	public void setSujet(String sujet) {
		this.sujet = sujet;
	}
	public String getTuteur() {
		return tuteur;
	}
	public void setTuteur(String tuteur) {
		this.tuteur = tuteur;
	}
	
	public Formation(int stock, String nom, String statut,
			Challenge challenge, Date dateDebut, Date dateFin, int nbrLimite,
			String sujet, String tuteur) {
		//super(numero, stock, nom, statut, challenge);
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nbrLimite = nbrLimite;
		this.sujet = sujet;
		this.tuteur = tuteur;
	}
	public Formation() {
		super();
	}
}
