package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Utilisateur implements Serializable{
	@Id @GeneratedValue
	private Long code;
	private String nom,prenom;
	private String email;
	@OneToMany(mappedBy="utilisateur",fetch=FetchType.LAZY)
	private Collection<Mission> Missions;
	@OneToMany(mappedBy="utilisateur",fetch=FetchType.LAZY)
	private Collection<Tache> taches;
	public Utilisateur() {
		super();
	}
	public Utilisateur(String nom, String prenom, String email) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
	}
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Collection<Mission> getMissions() {
		return Missions;
	}
	public void setMissions(Collection<Mission> missions) {
		Missions = missions;
	}
	
	
	

}
