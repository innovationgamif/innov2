package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Mission implements Serializable{
	public Collection<Tache> getTaches() {
		return taches;
	}
	public void setTaches(Collection<Tache> taches) {
		this.taches = taches;
	}
	@Id
	private String codeMission;
	private Date dateCreation;
	private String etat;
	private String objectif;
	private String desciptif;
	@ManyToOne
	@JoinColumn(name="code_util")
	private Utilisateur utilisateur;
	@OneToMany(mappedBy="mission",fetch=FetchType.LAZY)
	private Collection<Operation> operations;
	@OneToMany(mappedBy="mission",fetch=FetchType.LAZY)
	private Collection<Tache> taches;
	public Mission() {
		super();
	}
	public Mission(String codeMission, Date dateCreation, String etat,
			Utilisateur utilisateur) {
		super();
		this.codeMission = codeMission;
		this.dateCreation = dateCreation;
		this.etat = etat;
		this.utilisateur = utilisateur;
	}
	public String getCodeMission() {
		return codeMission;
	}
	public void setCodeMission(String codeMission) {
		this.codeMission = codeMission;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public Collection<Operation> getOpérations() {
		return operations;
	}
	public void setOpérations(Collection<Operation> opérations) {
		this.operations = opérations;
	}
	
	
}
