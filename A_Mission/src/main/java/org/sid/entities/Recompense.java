

package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Recompense implements Serializable{
	
		@Id @GeneratedValue
		private Long numero;
		private int stock;
		private String nom;
		private String statut;
		
		@OneToMany
		@JoinColumn(name="codeChg")//nom du clé étrangaire
		private Collection<Challenge> challenge;
		
		public Recompense() {
		}

		public Long getNumero() {
			return numero;
		}

		public void setNumero(Long numero) {
			this.numero = numero;
		}

		public int getStock() {
			return stock;
		}

		public void setStock(int stock) {
			this.stock = stock;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getStatut() {
			return statut;
		}

		public void setStatut(String statut) {
			this.statut = statut;
		}

		public Recompense(Long numero, int stock, String nom, String statut, Collection<Challenge> challenge) {
			super();
			this.numero = numero;
			this.stock = stock;
			this.nom = nom;
			this.statut = statut;
			this.challenge = challenge;
		}

}
