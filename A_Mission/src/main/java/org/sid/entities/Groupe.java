package org.sid.entities;

import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Groupe {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codeGroupe;
	private String nomGroupe;
	
	@OneToMany
	private Collection<Challenge> challenge;
	
	@OneToMany
	private Collection<Collaborateur> collaborateur;
	
/*
	//attributs d'association
	@ManyToMany(mappedBy="groupes")  //çàd l'association est décrite pour l'attribut  $$$ 'groupes' $$$ de la classe 'Collaborateur' qui représente le type de la collection--> si non on fait comme suit:
	//@JoinTable(name="EMP_GR", joinColumns=@JoinColumn(name="CODE_GR")  ,inverseJoinColumns=@JoinColumn(name="CODE_EMP")   )
	
	private Collection<Collaborateur> collaborateurs; //un groupe contient plusieurs collaborateurs
	
	*/
	public Groupe(String nomGroupe) {
		super();
		this.nomGroupe = nomGroupe;
	}

	public int getCodeGroupe() {
		return codeGroupe;
	}

	public void setCodeGroupe(int codeGroupe) {
		this.codeGroupe = codeGroupe;
	}

	public String getNomGroupe() {
		return nomGroupe;
	}

	public void setNomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}

}
