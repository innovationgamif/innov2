package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Collaborateur implements Serializable{

	@Id @GeneratedValue
	private Long matricule;
	private String nom;
	private String email;

	@OneToMany 
	private Collection<Groupe> groupe;
	
	public Collaborateur(String nom, String email) {
		super();
		this.nom = nom;
		this.email = email;
	}
	public Collaborateur(Long matricule, String nom, String email) {
		super();
		this.matricule = matricule;
		this.nom = nom;
		this.email = email;
	}

	public Collaborateur() {
		super();
	}

	public Long getMatricule() {
		return matricule;
	}

	public void setCode(Long matricule) {
		this.matricule = matricule;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
